/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./build/*html", "./build/js/*.js"],
  theme: {
    extend: {
      screens: {
        'widescreen': { 'raw': '(min-aspect-ratio: 3/2)'},
        'tallscreen': { 'raw': '(max-aspect-ratio: 13/20)'},
      },
      keyframes: {
        "open-menu" : {
          "0%" : { transform: 'scaleY(0)'},
          // "80%" : { transform: 'scaleY(1.2)'},
          "100%" : { transform: 'scaleY(1)'},
        },

        typing: {
          "0%": {
            width: "0%",
            visibility: "hidden"
          },
          "100%": {
            width: "100%"
          },
        },
        blink: {
          "50%": {
            borderColor: "white"
          },

          "100%": {
            borderColor: "transparent",
          }
        }
      },
      animation: {
        typing: "typing 2s steps(20), blink .7s infinite",
        spinning: "spin .5s 1",
        'open-menu' : "open-menu 0.5s ease-in-out forwards",
      }
    }
  },
  plugins: [],
}
